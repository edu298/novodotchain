using System.ComponentModel.DataAnnotations;
using DotChain.Api.Models.Blockchain;
using DotChain.Api.Models.Wallets;
using DotChain.Models;
using Microsoft.AspNetCore.Mvc;

namespace DotChain.Api.Controllers;

[ApiController]
[Route("api/v1/[controller]")]
public class BlockchainController : ControllerBase
{
    private readonly Blockchain _blockchain;
    private readonly IWalletRepository _repository;

    public BlockchainController(Blockchain blockchain, IWalletRepository repository)
    {
        _blockchain = blockchain;
        _repository = repository;
    }
    
    /// <summary>
    /// Creates a message to validation
    /// </summary>
    /// <param name="address"></param>
    /// <returns></returns>
    [HttpPost("requestValidation")]
    [ProducesResponseType(typeof(BlockMessage), 200)]
    public IActionResult GetBlockMessage([Required] [FromForm] string address)
    {
        var blockMessage = _blockchain.RequestMessageOwnershipVerification(address);
        return Ok(blockMessage);
    }

    /// <summary>
    /// Submit a star
    /// </summary>
    /// <param name="body"></param>
    /// <returns></returns>
    [HttpPost("submitstar")]
    [ProducesResponseType(typeof(Block), 200)]
    [ProducesResponseType(typeof(string[]), 422)]
    public IActionResult Submitstar([FromBody] BlockStarInput body)
    {
        var wallet = _repository.Get(body.Address);
        if (wallet is null)
            return NotFound("wallet not found");

        var result = _blockchain.SubmitStar(new BlockMessage(body.Message), body.Signature, wallet.PublicKey, body.Star);

        if (result.block is null && result.errors.Any())
            return UnprocessableEntity(result.errors);

        return Ok(result.block);
    }
    
    /// <summary>
    /// Find block by height
    /// </summary>
    /// <param name="height"></param>
    /// <returns></returns>
    [HttpGet("block/{height:int}")]
    [ProducesResponseType(typeof(Block), 200)]
    [ProducesResponseType(404)]
    public IActionResult Get(int height)
    {
        var block = _blockchain.GetBlockByHeight(height);
        if (block is null)
            return NotFound("block not found");

        return Ok(block);
    }

    /// <summary>
    /// Find block by hash
    /// </summary>
    /// <param name="hash"></param>
    /// <returns></returns>
    [HttpGet("block/hash/{hash}")]
    [ProducesResponseType(typeof(Block), 200)]
    [ProducesResponseType(404)]
    public IActionResult GetBlockByHash(string? hash)
    {
        if (hash is null)
            return NotFound("Block Not Found! Review the Parameters!");

        var block = _blockchain.GetBlockByHash(hash);
        if (block is null)
            return NotFound("Block Not Found!");
        
        return Ok(block);
    }
    
    /// <summary>
    /// Find blockstars by address
    /// </summary>
    /// <param name="address"></param>
    /// <returns></returns>
    [HttpGet("block/address/{address}")]
    [ProducesResponseType(typeof(BlockStar[]), 200)]
    [ProducesResponseType(404)]
    public IActionResult GetStarsByOwner(string? address)
    {
        if (address is null)
            return NotFound("Block Not Found! Review the Parameters!");

        var stars = _blockchain.GetStarsByWalletAddress(address);
        if (!stars.Any())
            return NotFound("Block Not Found!");
        
        return Ok(stars);
    }
}