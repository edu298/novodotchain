using DotChain.Api.Models.Wallets;
using Microsoft.AspNetCore.Mvc;

namespace DotChain.Api.Controllers;

[ApiController]
[Route("api/v1/[controller]")]
public class WalletController : ControllerBase
{
    private readonly IWalletRepository _repository;

    public WalletController(IWalletRepository repository)
    {
        _repository = repository;
    }

    /// <summary>
    /// Creates a new wallet
    /// </summary>
    /// <returns></returns>
    [HttpPost]
    [ProducesResponseType(typeof(WalletResult), 201)]
    public IActionResult Create()
    {
        var wallet = new Wallet();
        _repository.Add(wallet);
        return Created($"/{wallet.PublicAddress}", new WalletResult { Wallet = wallet, Result = "Wallet was created!!!" });
    }

    /// <summary>
    /// Signs a message with a private key
    /// </summary>
    /// <param name="data">data to sign</param>
    /// <param name="address">public address</param>
    /// <returns></returns>
    [HttpPost("{address}/sign")]
    [ProducesResponseType(typeof(WalletResult), 200)]
    [ProducesResponseType(404)]
    public IActionResult Sign([FromForm] string data, string address)
    {
        var wallet = _repository.Get(address);
        if (wallet is null)
            return NotFound("wallet not found");

        var signature = wallet.Sign(data);
        return Ok(new WalletResult { Wallet = wallet, Result = signature });
    }

    /// <summary>
    /// Verifies signature
    /// </summary>
    /// <param name="message">plain text</param>
    /// <param name="signature">message signed</param>
    /// <param name="address">public address</param>
    /// <returns></returns>
    [HttpPost("{address}/verify")]
    [ProducesResponseType(typeof(WalletResult), 200)]
    [ProducesResponseType(400)]
    [ProducesResponseType(404)]
    public IActionResult Verify([FromForm] string message, [FromForm] string signature, string address)
    {
        var wallet = _repository.Get(address);
        if (wallet is null)
            return NotFound("wallet not found");

        return wallet.Verify(message, signature) 
            ? Ok(new WalletResult { Wallet = wallet, Result = "Signature is valid" }) 
            : BadRequest();
    }

    /// <summary>
    /// Get wallet
    /// </summary>
    /// <param name="address">public address</param>
    /// <returns></returns>
    [HttpGet("{address}")]
    [ProducesResponseType(typeof(WalletResult), 200)]
    [ProducesResponseType(404)]
    public IActionResult Get(string address)
    {
        var wallet = _repository.Get(address);
        if (wallet is null)
            return NotFound("wallet not found");

        return Ok(new WalletResult { Wallet = wallet, Result = "OK" });
    }
}