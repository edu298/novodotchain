namespace DotChain.Api.Models.Wallets;

public record WalletResult
{
    public Wallet? Wallet { get; init; }
    public string? Result { get; init; }
}