using System.Diagnostics.CodeAnalysis;
using System.Security.Cryptography;
using System.Text;

namespace DotChain.Extensions;

[ExcludeFromCodeCoverage]
public static class EncodingExtensions
{
    public static string ToSha256(this object data)
    {
        using var sha256 = SHA256.Create();
        var json = data.ToJson();
        var bytes = Encoding.UTF8.GetBytes(json);

        return sha256.ComputeHash(bytes).ToHexString();
    }

    public static string ToBase64(this byte[] bytes) =>
        Convert.ToBase64String(bytes);
    
    public static byte[] FromBase64(this string value) =>
        Convert.FromBase64String(value);
    
    public static string ToHexString(this object data)
    {
        var json = data.ToJson();
        var bytes = Encoding.UTF8.GetBytes(json);
        return bytes.ToHexString();
    }
    
    public static string ToHexString(this byte[] bytes)
    {
        var sb = new StringBuilder();
        foreach (var t in bytes)
        {
            sb.Append(t.ToString("x2"));
        }

        return sb.ToString();
    }

    public static T? FromHexString<T>(this string hexString)
    {
        var bytes = new byte[hexString.ToUpper().Length / 2];
        for (var i = 0; i < bytes.Length; i++)
        {
            bytes[i] = Convert.ToByte(hexString.ToUpper().Substring(i * 2, 2), 16);
        }

        return Encoding.UTF8.GetString(bytes).FromJson<T>();
    }
}